#!/usr/bin/env bash

# Network user authentication
# Tony Garcia <tony.garcia@rackspace.com>
#
#~ Options:
#~   -h    Print this help
#~   -l    Action: Login
#~   -d    Define datacenter [dfw,hkg,iad,lon3,lon5,ord,syd]
#~   -s    Action: Login status.
#~   -o    Action: Logout.
#~   -u    RSA username.
#~   -v    Print the output of the Action.
#~   -V    Print the Version.
#~
#~ If verbose(-v) is omitted for an Action, the exit codes can be used to
#~ find the evaluation status of such action.
#~
#~ Exit Codes:
#~    0    Success
#~    1    Failed

_VERSION=0.1

print_help(){
  echo "Usage: $(basename $0) -d DC [-hlosvV]"
  grep -E '^#~' $0|sed -e 's/^#~//'
}

get_token() {
    rsa_token=$(/usr/local/bin/stoken)
}

get_endpoint() {
  local dc=${1}
  local auth_endpoint="auth.DC.gateway.rackspace.com"
  case ${dc} in
    dfw*)
      auth_endpoint=${auth_endpoint/DC/dfw1}
      ;;
    ord*)
      auth_endpoint=${auth_endpoint/DC/ord1}
      ;;
     lon|lon3)
      auth_endpoint=${auth_endpoint/DC/lon3}
      ;;
    lon5)
      auth_endpoint=${auth_endpoint/DC/lon5}
      ;;
    iad*)
      auth_endpoint=${auth_endpoint/DC/iad3}
      ;;
    hkg*)
      auth_endpoint=${auth_endpoint/DC/hkg1}
      ;;
    syd*)
      auth_endpoint=${auth_endpoint/DC/syd2}
      ;;
  esac
  echo ${auth_endpoint}
}

network_status(){
  local endpoint=${1}
  local verbose=${2}
  local url_login="https://${endpoint}/netaccess/connstatus.html"
  local response=$(curl -XGET -sk "${url_login}" | grep 'table')
  local login_status=$(awk -F'>' '{gsub("<.*", ""); print $14, $15, $16, $17}' <<<"${response}")
  local is_login_valid=$(awk -F'>' '/table/ {print $15}' <<<"${response}"| sed -e 's/<.*//')
  if [[ ${is_login_valid} == 'You are not logged in.' ]]; then
    if [[ ! -z ${verbose} ]]; then
      echo "Not logged in."
    fi
    return 1
  else
    if [[ ! -z ${verbose} ]]; then
      is_login_valid=$(awk -F'>' '/table/ {print $12}' <<<"${response}" | sed -e 's/<.*//')
      logged_username=$(awk -F'>' '/table/ {print $14}' <<<"${response}" | sed -e 's/<.*//')
      logged_ip=$(awk -F'>' '/table/ {print $15}' <<<"${response}" | sed -e 's/<.*//')
      logged_since=$(awk -F'>' '/table/ {print $16}' <<<"${response}" | sed -e 's/<.*//')
      logged_long=$(awk -F'>' '/table/ {print $17}' <<<"${response}" | sed -e 's/<.*//')
      echo -e "${is_login_valid}\n${logged_username}\n${logged_ip}\n${logged_since} ${logged_long}"
    fi
    return 0
  fi
}

network_login(){
  get_token
  echo ${get_token}
  local endpoint=${1}
  local verbose=${2}
  local username=${3}
  local url_login="https://${endpoint}/netaccess/loginuser.html"
  if [[ -z ${username} ]]; then
      read -p 'RSA username: ' username
  fi
  echo
  local payload="username=${username}&password=${rsa_token}&Login=Continue"
  local url_pre_login="https://${endpoint}/netaccess/connstatus.html"
  # Authenticate
  local response=$(curl -s -XPOST -d 'login=Log%20In%20Now' "${url_pre_login}" \
    &>/dev/null && curl -XPOST -s -d "${payload}" "${url_login}")
  if grep -q 'Credentials Rejected' <<<"${response}"; then
    if [[ ! -z ${verbose} ]]; then
      echo "Credentials Rejected."
    fi
    return 1
  elif grep -q 'Wait for token to change,' <<<"${response}"; then
    while [[ -z ${auth} ]]; do
      read -sp 'Wait for token to change and type it again\nNext Code RSA: ' rsa_token
      url_login="https://${endpoint}/netaccess/loginmore.html"
      payload="response=${rsa_token}"
      response=$(curl -XPOST -s -d "${payload}" "${url_login}")
      if grep -q 'You are logged in.' <<<"${response}"; then
        auth='OK'
      elif grep -q 'Credentials Rejected' <<< ${response}; then
        if [[ ! -z ${verbose} ]]; then
          echo "Credentials Rejected."
        fi
        return 1
      fi
    done
  fi
  return 0
}
 
network_logout(){
  local endpoint=${1}
  local verbose=${2}
  local url_login="https://${endpoint}/netaccess/connstatus.html"
  local payload='sid=0&logout=Log+Out+Now'
  local response=$(curl -XPOST -sk -d "${payload}" "${url_login}")
  if grep -q "You are not logged in" <<<"${response}"; then
    if [[ ! -z ${verbose} ]]; then
      echo "You are now logged out."
    fi
    return 0
  else
    return 1
  fi
}

verbose=''
while getopts d:hlosu:vV flag; do
  case ${flag} in
    d)
      dc=$(echo ${OPTARG}|tr '[:upper:]' '[:lower:]')
      ;;
    l)
      action=network_login
      ;;
    o)
      action=network_logout
      ;;
    s)
      action=network_status
      ;;
    u)
      username=${OPTARG}
      ;;
    v)
      verbose=1
      ;;
    V)
      echo ${_VERSION}
      exit 0
      ;;
    h|*)
      print_help
      exit 1
      ;;
 esac
done

if [[ -z ${dc} ]]; then
  echo "Error: -d DC is required"
  print_help
  exit 1
fi
endpoint=$(get_endpoint ${dc})
case ${action} in
  network_status)
    if ! network_status "${endpoint}" "${verbose}"; then
      exit 1
    fi
    ;;
  network_login)
    if network_status "${endpoint}" "${verbose}"; then
      exit 0
    fi
    if ! network_login "${endpoint}" "${verbose}" "${username}"; then
      exit 1
    fi
    network_status "${endpoint}" "${verbose}"
    ;;
  network_logout)
    if ! network_logout "${endpoint}" "${verbose}"; then
      exit 1
    fi
    ;;
  *)
    echo "Error: An action is required -l|-o|-s"
    print_help
    exit 1
esac
exit 0
