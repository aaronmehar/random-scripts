#!/usr/bin/env bash

PROD=$(cut -d ',' -f 1 < /root/sre/001_ServerDeployment-production.csv | grep -iE 'comp')
STAG=$(cut -d ',' -f 1 < /root/sre/001_ServerDeployment-staging.csv | grep -iE 'comp')

SSH="ssh -o StrictHostKeyChecking=no -i /root/.ssh/fennel.key"
SCP="scp -i /root/.ssh/fennel.key"

function crypt_close() {
	$SSH $1 "bash /tmp/crypt_close.sh" &> /root/sre/scary_please_do_not_run/log/${1}.log
}

for SERVER in $PROD
do
        # 126, IO ERRORS
        # 161, HOST DOWN
        # 203, DISK LAYOUT DIFF?!?
        if [ "$SERVER" == "S1-PRD-COMP-126" ] || [ "$SERVER" == "S1-PRD-COMP-161" ] || [ "$SERVER" == "S1-PRD-COMP-203" ]
        then
                continue
        fi

        # NUMBER=$(echo $SERVER | cut -d '-' -f 4)
        # if [ $NUMBER -gt 40 ]
        # then
        #         continue
        # fi

        TYPE=$(echo $SERVER | cut -d '-' -f 3)
        if [ "$TYPE" == "COMP" ]
        then
		echo $SERVER
		crypt_close "$SERVER" &
	fi
done
