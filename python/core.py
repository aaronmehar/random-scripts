#!/usr/bin/env python

import requests
import exceptions
from requests.exceptions import HTTPError, Timeout
import json
import sys

class TicketAdmin():

    def generic(self, payload, token):
        queryurl = 'https://core.rackspace.com/ctkapi/query'
        headers = {'X-Auth-Token': token,
                   'content-type': 'application/json'}
        try:
            r = requests.post(queryurl, data=json.dumps(payload),
                              headers=headers)
            r.raise_for_status()
            return r.json()
        except Timeout:
            print "Request timed out, please try again"
        except HTTPError:
            if r.status_code == 400:
                print "Invalid Request, check what you sent to the API"
                return None
            elif r.status_code == 500:
                print "Error 500: Probably permissions issue, or CORE is dead"
                return None
            elif r.status_code == 503:
                print "CORE is throttling you - calm down, and try in a bit"
                return None
        except requests.exceptions.ConnectionError:
            print "No internet connectivity."
            raise

    def create(self, token, account, queue, source, severity, subcategory, subject, comment):
        payload = {"class": "Account.Account", "load_arg": account,
                   "method": "addTicket", "args": [queue, subcategory,
                                                   source, severity,
                                                   subject, comment],
                   "result_map": {"status": "status.id", "account":
                                  "account.number", "number": "number"}}
        return self.generic(payload, token)

    def publicupdate(self, token, ticket, comment):
        payload = {"class": "Ticket.Ticket", "load_arg": ticket, "method":
            "addMessage", "args": [], "keyword_args":
                       {'text': comment, 'source': 3, 'private': 'False',
                        'has_bbcode': True, 'private': False}}
        return self.generic(payload, token)

    def privateupdate(self, token, ticket, comment):
        payload = {"class": "Ticket.Ticket", "load_arg": ticket, "method":
            "addMessage", "args": [], "keyword_args":
                       {'text': comment, 'source': 3, 'has_bbcode': True,
                        'private': True}}
        return self.generic(payload, token)

    def status(self, token, ticket, status):
        payload = {"class": "Ticket.Ticket", "load_arg": ticket,
                   "method": "setStatusByName", "args": [status],
                   "keyword_args": {}}
        return self.generic(payload, token)

    def query(self, token, payload):
        return self.generic(payload, token)

class SSLTool():

    def gencsr(self, token, payload):
        queryurl = 'https://api.ssltool.rackspace.com/gencsr'
        headers = {'X-Auth-Token': token,
                   'content-type': 'application/json'}
        try:
            r = requests.post(queryurl, data=json.dumps(payload),
                              headers=headers)
            r.raise_for_status()
            return r.json()
        except Timeout:
            print "Request timed out, please try again"
        except HTTPError:
            if r.status_code == 400:
                print "Invalid Request, check your payload is valid"
                return None
        except requests.exceptions.ConnectionError:
            print "No internet connectivity."
            raise


"""
ticket Source
[1, "Phone Call", "Received info from customer via phone call"],
[2, "MyRackspace", "Customer added ticket info on MyRackspace.com"],
[3, "Rackspace Employee", "A Rackspace employee added their own info"],
[4, "CORE Automated", "CORE generated a ticket info automatically"],
[5, "Genie", "A Rackspace Employee making a request of another internal department"],
[6, "Mass Ticket", "A bulk-generated ticket"],
[7, "Walk-Up", "A Rackspace Employee walking up to an internal department to make a request"],
[8, "EZCASH", "Ticket generated via EZCASH webservice"],
[9, "Email", "Customer request started from an email"],
[10, "Other", "Unspecified ticket source"],
[11, "Chat Support", "Ticket originated during a chat support session"],
[12, "RunBook", "RunBook Automation"],
[13, "RackerApp", "RackerApp"]

Ticket Subcategory
https://ws.core.rackspace.com/ctkapi/browse/Ticket/Subcategory

Ticket Severity
1	Standard
2	Urgent
3	Emergency
"""
