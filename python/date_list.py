from datetime import datetime, timedelta

def last_n_months(num_of_months, start_date=datetime.now(), include_curr=True):
    f = "%Y-%m-%d"
    curr = start_date
    if include_curr:
        yield curr.strftime(f)
    for num in range(num_of_months):
        curr = curr.replace(day=1) - timedelta(days=1)
        yield curr.strftime(f)

# This month and last 12 months.
print [m for m in last_n_months(24)]
