
from __future__ import print_function
import requests
import json
from random import randint

headers = {'User-Agent': 'motd ShowerThought'}

st = requests.get('https://www.reddit.com/r/showerthoughts/top.json',headers=headers )
data = json.loads(st.text)

print(data['data']['children'][randint(0, 25)]['data']['title'])

