from datetime import datetime, date
from pyinvoice.models import InvoiceInfo, ServiceProviderInfo, ClientInfo, Item, Transaction
from pyinvoice.templates import SimpleInvoice
import random

#doc = SimpleInvoice('invoice.pdf')
date = ['2018-02-28', '2018-01-31', '2017-12-31', '2017-11-30', '2017-10-31', '2017-09-30', '2017-08-31', '2017-07-31', '2017-06-30', '2017-05-31', '2017-04-30', '2017-03-31', '2017-02-28', '2017-01-31', '2016-12-31', '2016-11-30', '2016-10-31', '2016-09-30', '2016-08-31', '2016-07-31', '2016-06-30', '2016-05-31', '2016-04-30', '2016-03-31']

def GetInvoice(date):

    # Generate File Name
    filename = date + '_invoice.pdf'

    doc = SimpleInvoice(filename)

    # Generate Invoice Number
    InvoiceId = random.randint(1,10001)

    #def GetInvoice():
    doc.invoice_info = InvoiceInfo(InvoiceId, date)  # Invoice info, optional

    # Client info, optional
    doc.client_info = ClientInfo(name='Customer Capital Limited',
                                 street='123 Street Lane', city='London',
                                 state='London', post_code='EC2W 5QR')

    item_desc="""
    Intel Core2 Duo E4500
    3GB Memory
    160GB 7200rpm Hard Disk
    Debian 5.x x64
    Proactive Monitoring and Alerting SLA
    10 TB Bandwidth
    """
    # Add Item
    doc.add_item(Item('Starter Dedicated', item_desc, 1, '30.00'))

    # Tax rate, optional
    doc.set_item_tax_rate(20)  # 20%

    # Optional
    doc.set_bottom_tip("Email: aaron@mehar.me<br />Don't hesitate to contact us for any questions.")

    doc.finish()

for invdate in date:
    GetInvoice(invdate)
