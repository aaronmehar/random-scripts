#!/usr/bin/env python
from auth import Authenticate
from core import TicketAdmin
import sys
import argparse

# Grab myself a token for Internal Identity
auth = Authenticate()
token = auth.internal('token')
username = auth.coreusername
core = TicketAdmin()

parser = argparse.ArgumentParser()
parser.add_argument('-t', '--ticket',  help='Ticket ID from the request')
parser.add_argument('-o', '--objective',  help='Objective of the maintenance')
args = parser.parse_args()

comment = """
Hello,

Important information regarding IUS packages:

IUS is a Rackspace sponsored community project which provides continually
updated packages for newer versions of select software for RHEL and CentOS Linux
distributions. IUS is not a Rackspace Product, thus IUS packages are not covered
by any support service level agreements with Rackspace. Rackspace support teams
will assist in enabling and installing IUS packages on your behalf.

IUS packages are slightly different from official Red Hat packages in regards to
the security posture.  They are aligned with the direct upstream project and
leverage the continual security fixes as provided by their respective projects,
as opposed to Red Hat's development model of backporting security fixes.  Issues
with IUS packages should be reported directly to the project.

    https://ius.io/#contact-us

IUS packages are pinned to a specific upstream major version, and will not
automatically update to the next major version. This will normally result in a shorter 
lifecycle than their stock RHEL equivalents (Since Red Hat continues to backport 
security patches beyond upstream EOL). For further information, please see:

   https://ius.io/LifeCycle/

IUS has a low traffic GitHub repository that includes important information including
EOL announcements.  It is recommended that you watch the following repository:

   https://github.com/iuscommunity/announce

Additional information:

   https://ius.io
   https://ius.io/SafeRepo/
   https://wiki.centos.org/AdditionalResources/Repositories
   https://dl.iuscommunity.org/pub/ius/IUS-COMMUNITY-EUA
   https://access.redhat.com/security/updates/backporting

Thanks
Aaron
"""

core.publicupdate(token, args.ticket, comment)
core.status(token, args.ticket, 'REQUIRE Feedback')
