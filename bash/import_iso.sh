#!/usr/bin/env bash
set -euo pipefail

if [ "$EUID" -ne 0 ]; then
  echo "Error: must be ran as root"
  exit 1
fi

if [ "$@1" = "" ]; then
  echo "ISO path must be given, i.e. ./import-yum-updates.sh /path/to/file.iso"
  exit 1
fi

if [[ ! "$@" =~ .*centos7.* ]]; then
  echo "Error: this script must only be used for a CentOS 7 ISO"
  exit 1
fi

case "$1" in
  *-epel*.iso)
    repo="centos7-x86_64-epel"
  ;;
  *-extras*.iso)
    repo="centos7-x86_64-extras"
  ;;
  *-os*.iso)
    repo="centos7-x86_64-os"
  ;;
  *-updates*.iso)
    repo="centos7-x86_64-updates"
  ;;
  *)
    echo "Unrecognised ISO type!"
    exit 1
  ;;
esac

echo "Updating repo: $repo"

echo "Unmounting /mnt/iso"
mkdir -p /mnt/iso
umount /mnt/iso > /dev/null 2>&1 || true

echo "Mounting $1"
mount -t iso9660 -o loop "$1" /mnt/iso

echo "Copying from /mnt/iso/$repo/* to /opt/htrepo/$repo/."
cp -r "/mnt/iso/$repo/"* "/opt/htrepo/$repo/."

echo "Rebuilding repo"
createrepo --update "/opt/htrepo/$repo"

echo "Unmounting /mnt/iso"
mkdir -p /mnt/iso
umount /mnt/iso > /dev/null 2>&1 || true

