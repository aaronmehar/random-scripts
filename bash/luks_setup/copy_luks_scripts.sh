#!/usr/bin/env bash

PROD=$(cut -d ',' -f 1 < /root/sre/001_ServerDeployment-production.csv | grep -iE 'comp')
STAG=$(cut -d ',' -f 1 < /root/sre/001_ServerDeployment-staging.csv | grep -iE 'comp')

SSH="ssh -o StrictHostKeyChecking=no -i /root/.ssh/fennel.key"
SCP="scp -i /root/.ssh/fennel.key"

for SERVER in $PROD
do
        TYPE=$(echo $SERVER | cut -d '-' -f 3)
        if [ "$TYPE" == "COMP" ]
        then
		echo $SERVER
		$SCP /root/sre/scary_please_do_not_run/create* $SERVER:/tmp
		$SCP /root/sre/scary_please_do_not_run/crypt_close.sh $SERVER:/tmp
	fi
done
