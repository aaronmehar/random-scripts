#!/usr/bin/env python

import requests
import json

## Get my current IP address
cur_ip = requests.get('http://4.icanhazip.com')

cfURL = 'https://api.cloudflare.com/client/v4'
token = ''
email = ''

## Cloudflare Domain variables
domain = 'mehar.me'
record = 'home'
headers = {'X-Auth-Email': email,
           'X-Auth-Key': token,
           'Content-Type': 'application/json'
          }

zones = requests.get(cfURL + '/zones', headers=headers)
result = json.loads(zones.text)


