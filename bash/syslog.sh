function slog() {
    Username=$(whoami)
    Domain='mydomain.com'

    if [ -z "$1" ] || [ -z "$2" ]
    then
        echo -e "Provide service and region. Usage:\nslog [service] [region]\nServices:\nservices, compute, xenserver, systems, scheduler, logstash\nRegions:\ndfw, iad, ord, hkg, syd, lon"
        return 1
    else
        Endpoint=$1
        Region=$2
    fi
    case ${Endpoint} in
        services|compute|xenserver|systems|scheduler|logstash)
            true
            ;;
        *)
            echo "Wrong endpoint. Available endpoints: services, compute, xenserver, systems, scheduler, logstash"
            return 1
            ;;
    esac
    case ${Region} in
        dfw|iad|ord|hkg|syd|lon)
            true
            ;;
        *)
            echo "Wrong region. Available regions: dfw, iad, ord, hkg, syd, lon"
            return 1
            ;;
    esac
    ssh ${Username}@syslog-${Endpoint}01.global.${Region}.${Domain}
}

