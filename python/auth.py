#!/usr/bin/env python

import requests
import exceptions
from requests.exceptions import HTTPError, Timeout
import json
import yaml
from yaml import YAMLError
import sys
import keyring


class Authenticate():

    def __init__(self):
        try:
            creds = yaml.load(file('credentials.yml', 'r'))
        except YAMLError as e:
            print "Error in configuration file:", e
            raise
        try:
            self.coreusername = creds['sso']['user']
            self.corepassword = creds['sso']['pass']
        except KeyError as e:
            print "SSO credentials missing from credentials.yml"
        try:
            self.rscloudusername = creds['rscloud']['user']
            self.rscloudpassword = creds['rscloud']['pass']
        except KeyError as e:
            print "Cloud credentials are missing from credentials.yml"
        try:
            self.thawteusername = creds['thawte']['user']
            self.thawtepassword = creds['thawte']['pass']
            self.thawtepartnercode = creds['thawte']['partnercode']
        except:
            pass
        return

    def auth_request(self, payload, auth_url, auth_token=None):
        headers = {'Content-Type': 'application/json'}
        try:
            r = requests.post(auth_url, data=json.dumps(payload),
                              headers=headers)
            r.raise_for_status()
            if auth_token:
                auth_token = r.json()
                return auth_token['access']['token']['id']
            else:
                return r.json()
        except Timeout:
            print "Authentication request timed out, please try again"
            return None
        except HTTPError:
            if r.status_code == 401:
                print "Unable to authenticate with credentials provided"
                return None
            if r.status_code == 400:
                print "Invalid JSON sent, please fix and try again"
                return None
            elif r.status_code == 500:
                print "Error 500: Probably permissions issue"
                return None
        except requests.exceptions.ConnectionError:
            print "No internet connectivity."
            raise

    def internal(self, auth_token=None):
        auth_url = 'https://identity-internal.api.rackspacecloud.com/v2.0/tokens'
        payload = {"auth": {"RAX-AUTH:domain": {"name": "Rackspace"},
                            "passwordCredentials":
                                {"username": self.coreusername,
                                 "password": self.corepassword}}}
        if auth_token:
            return self.auth_request(payload, auth_url, auth_token)
        else:
            return self.auth_request(payload, auth_url)

    def rscloud(self, auth_token=None):

        auth_url = 'https://identity.api.rackspacecloud.com/v2.0/tokens'
        payload = {"auth": {"RAX-KSKEY:apiKeyCredentials":
                            {"username": self.rscloudusername, "apiKey": self.rscloudpassword}}}
        if auth_token:
            return self.auth_request(payload, auth_url, auth_token)
        else:
            return self.auth_request(payload, auth_url)
