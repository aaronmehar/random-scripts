#!/usr/bin/env bash

# Pull in SSO Details
source ~/.authdetails.conf

doWork() {
    unset exit_code
    rackerAuthURL="https://identity-internal.api.rackspacecloud.com/v2.0"
    getRackerToken "$sso_user" "$sso_pass"
    unset sso_user sso_pass
    [ $exit_code ] && return
}

getRackerToken() { #sso_user sso_pass
    curl_output=$(curl -s $rackerAuthURL/tokens -XPOST -H "Content-Type: application/json" -d '{"auth": {"RAX-AUTH:domain": {"name": "Rackspace"},"passwordCredentials": {"username": "'"$1"'","password": "'"$2"'"}}}' | python -mjson.tool)
    ret=$?
    if [[ $ret != 0 ]];then
        printf "Error: couldn't get rackerToken -- curl error %d\n%s\n" $ret $curl_output
    else
        raxToken=$(echo ${curl_output} | python -c 'import sys,json;data=json.loads(sys.stdin.read());print data["access"]["token"]["id"]')
    fi
    unset curl_output ret uri
}

doWork
