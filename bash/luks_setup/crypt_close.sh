#!/usr/bin/env bash

for hdfs_mount in $(ls -1 /hdfs/)
do
        umount /hdfs/${hdfs_mount}
done

for hdfs_crypt in $(ls -1 /dev/mapper/hdfs*)
do
        cryptsetup close $hdfs_crypt
done
