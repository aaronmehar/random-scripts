#!/usr/bin/env bash

# esnure compute host
hostname | grep -iq comp
if [ $? -ne 0 ]
then
	echo "This is not a compute host, exiting."
	echo "Please be really careful, this will nuke everything."
	exit 1
fi

# ensure one pvs
PVS_COUNT=$(pvs | sed '1d' | wc -l)
if [ $PVS_COUNT -ne 1 ]
then
        echo "More than one PVS detected, please check!"
        exit 1
fi

# ensure root is on lvm
ROOT_LVM=$(lvdisplay | grep -q /dev/vg/root)
if [ $? -ne 0 ]
then
        echo "The root partition is not on lvm, please check!"
        exit 1
fi

# find where boot is
BOOT_DISK=$(mount | grep '/boot ' | awk '{print $1}' | tr -d '2')

# ensure boot and luks are on same disk
PVS_LS=$(pvs | sed '1d' | awk '{print $1}')
LUKS_DISK=$(cryptsetup status ${PVS_LS} | grep 'device:' | awk '{print $NF}' | tr -d 3)

if [ "$BOOT_DISK" != "$LUKS_DISK" ]
then
        echo "boot and LUKS do not match, please check!"
        exit 1
fi

# all disks - less BOOT and LUKS
ALL_DISKS=$(ls -1 /dev/sd* | grep -v '[1-9]$' | grep -v $LUKS_DISK)

# loop
for DISK in $ALL_DISKS
do
	# make super sure!
	if [ "${DISK}" == "/dev/sdn" ]
	then
		continue
	fi

	# partition
	# echo "Partition $DISK."
	# parted -a optimal $DISK -s mklabel gpt mkpart hdfs 0% 100% && partprobe
	# if [ $? -ne 0 ]; then echo "ERROR PARTED - $DISK"; exit 1; fi

	# luks format
	LUKS_NAME="hdfs_$(echo $DISK | rev | cut -c 1)"
	echo "LUKS create $LUKS_NAME on ${DISK}1."
	cryptsetup -q luksFormat ${DISK}1 --key-file /root/hdfs_luks.key
	if [ $? -ne 0 ]; then echo "ERROR LUKS - $DISK"; exit 1; fi
	cryptsetup open ${DISK}1 ${LUKS_NAME} --type luks --key-file /root/hdfs_luks.key

	# check crypttab, echo if not exist
	grep -q $LUKS_NAME /etc/crypttab
	if [ $? -ne 0 ]
	then
		UUID=$(lsblk -p -o name,uuid | grep ${DISK}1 | awk '{print $NF}')
		echo "$LUKS_NAME UUID=${UUID} /root/hdfs_luks.key" >> /etc/crypttab
	fi

	# xfs format
	echo "Format ${LUKS_NAME} as XFS."
	mkfs.xfs -Kq /dev/mapper/${LUKS_NAME}
	if [ $? -ne 0 ]; then echo "ERROR FORMAT - $DISK"; exit 1; fi

	# add to fstab
	grep -q "/dev/mapper/${LUKS_NAME}" /etc/fstab
	if [ $? -ne 0 ]
	then
		echo -ne "/dev/mapper/${LUKS_NAME}\t/hdfs/${LUKS_NAME}\t\txfs\tnoatime\t\t0 0\n" >> /etc/fstab
	fi

	# mount & chown
	mkdir -p /hdfs/${LUKS_NAME}
	mount /hdfs/${LUKS_NAME}
	chown -R palantir. /hdfs/${LUKS_NAME}
done
