#!/usr/bin/env bash

if [ -f /root/hdfs_luks.key ]
then
	echo "Key already created, exiting."
	exit 1
fi

# create key
dd if=/dev/random of=/root/hdfs_luks.key bs=512 count=4

# chmod key
chown root. /root/hdfs_luks.key
chmod 400 /root/hdfs_luks.key

# create backup
mkdir /root/luks_bak
cp /root/hdfs_luks.key /root/luks_bak
