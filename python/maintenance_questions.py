#!/usr/bin/env python
from auth import Authenticate
from core import TicketAdmin
import sys
import argparse

# Grab myself a token for Internal Identity
auth = Authenticate()
token = auth.internal('token')
username = auth.coreusername
core = TicketAdmin()

parser = argparse.ArgumentParser()
parser.add_argument('-t', '--ticket',  help='Ticket ID from the request')
parser.add_argument('-o', '--objective',  help='Objective of the maintenance')
args = parser.parse_args()

comment = """
Hello,

I am outlining the maintenance plan for your account, which includes:

        Documenting the steps necessary to perform the work
        Testing those steps
        Creating a roll-back plan

=====

1) Confirm this is our maintenance objective: {}
2) Provide your Point of Contact (POC) name and phone number for this maintenance:
   2.1) Before starting the maintenance, should we notify the POC by phone (in addition to ticket update)?
   2.2) If yes, is there an alternate contact if the POC cannot be reached - include their name and phone number?
   2.3) Should we proceed with the maintenance if the POC is not available by phone?
   2.4) Upon completion of the maintenance, should we notify the POC by phone (in addition to ticket update)?
3) What checks should be performed to confirm the maintenance objective was met and that the solution is functioning as expected (i.e. site, service, etc.)?
4) Provide any additional information you feel may be helpful:

Please note it is recommended you have your application team available to validate your application is completely functional after the maintenance is complete.

Thanks
Aaron
""".format(args.objective)

core.publicupdate(token, args.ticket, comment)
core.status(token, args.ticket, 'REQUIRE Feedback')
