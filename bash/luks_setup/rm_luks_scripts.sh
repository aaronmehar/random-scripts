#!/usr/bin/env bash

PROD=$(cut -d ',' -f 1 < /root/sre/001_ServerDeployment-production.csv | grep -iE 'comp')
STAG=$(cut -d ',' -f 1 < /root/sre/001_ServerDeployment-staging.csv | grep -iE 'comp')

SSH="ssh -o StrictHostKeyChecking=no -i /root/.ssh/fennel.key"
SCP="scp -i /root/.ssh/fennel.key"

for SERVER in $PROD
do
        TYPE=$(echo $SERVER | cut -d '-' -f 3)
        if [ "$TYPE" == "COMP" ]
        then
		echo $SERVER
		$SSH $SERVER "rm -v /tmp/create_key.sh"	
		$SSH $SERVER "rm -v /tmp/create_luks.sh"	
		$SSH $SERVER "rm -v /tmp/crypt_close.sh"	
		sleep 1
	fi
done
